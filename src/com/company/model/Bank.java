package com.company.model;

public class Bank {
    private String name;
    private double course;

    public Bank(String name, double course) {
        this.name = name;
        this.course = course;
    }

    public String getInfo() {
        return String.format("Название банка: \"%s\" Курс: %s ", name, course);
    }

    public String getName() {
        return name;
    }

    public double getCourse() {
        return course;
    }
}
