package com.company;

import com.company.model.Bank;
import com.company.model.BladeMarket;
import com.company.model.Exchanger;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Bank[] banks = new Bank[3];
        banks[0] = new Bank("MegaBank", 25);
        banks[1] = new Bank("AlphaBank", 25.2);
        banks[2] = new Bank("MiniBank", 25.4);

        Exchanger[] exchangers = new Exchanger[3];
        exchangers[0] = new Exchanger("BlackBank", 25);
        exchangers[1] = new Exchanger("WhiteBank", 25.2);
        exchangers[2] = new Exchanger("BWBank", 25.4);

        BladeMarket[] bladeMarkets = new BladeMarket[1];
        bladeMarkets[0] = new BladeMarket("BladeBank", 26);

        for (Bank bank : banks){
            System.out.println(bank.getInfo());
        }

        for (Exchanger exchanger : exchangers) {
            System.out.println(exchanger.getInfo());
        }

        for (BladeMarket bladeMarket : bladeMarkets) {
            System.out.println(bladeMarket.getInfo());
        }

        Scanner in = new Scanner(System.in);

        System.out.println("Введите количество валюты: ");
        int kolVal = Integer.parseInt(in.nextLine());

        System.out.println("Введите банк для обмена: ");
        String nameBank = in.nextLine();

        System.out.print("Количество в USD: ");
        for (int i = 0; i < banks.length; i++) {
            if (nameBank.equalsIgnoreCase(banks[i].getName())) {
                System.out.println((kolVal/banks[i].getCourse()/100*95));
            }
        }

        for (int i = 0; i < exchangers.length; i++) {
            if (nameBank.equalsIgnoreCase(exchangers[i].getName())) {
                System.out.println(kolVal/exchangers[i].getCourse());
            }
        }

        for (int i = 0; i < bladeMarkets.length; i++) {
            if (nameBank.equalsIgnoreCase(bladeMarkets[i].getName())) {
                System.out.println(kolVal/bladeMarkets[i].getCourse());
            }
        }
    }
}
